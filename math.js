const plus = (...arg) => {
    start = 0;
    for (let i = 0; i < arg.length; i++) start += arg[i];
    return start;
}
const multiply = (...arg) => {
    start = 1;
    for (let i = 0; i < arg.length; i++) start *= arg[i];
    return start;
}

module.exports = {
    plus,
    multiply
}